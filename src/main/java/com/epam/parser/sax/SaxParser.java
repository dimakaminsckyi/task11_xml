package com.epam.parser.sax;

import com.epam.model.Candy;
import com.epam.model.Ingredient;
import com.epam.model.Type;
import com.epam.model.Value;
import com.epam.parser.Constant;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SaxParser extends DefaultHandler {

    private List<Candy> candyList;
    private Candy candy;
    private Type type;
    private Value value;
    private List<Ingredient> ingredients;
    private String currentElement;

    public SaxParser() {
        candyList = new ArrayList<>();
    }

    public List<Candy> getCandyList() {
        return this.candyList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentElement = qName;
        switch (currentElement) {
            case Constant.CANDY_TAG:
                candy = new Candy();
                candy.setCandyId(Integer.parseInt(attributes.getValue(Constant.CANDY_ID_ATTRIBUTE)));
                break;
            case Constant.TYPE_TAG:
                type = new Type();
                break;
            case Constant.VALUE_TAG:
                value = new Value();
                break;
            case Constant.INGREDIENTS_TAG:
                ingredients = new ArrayList<>();
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName) {
            case Constant.CANDY_TAG:
                candyList.add(candy);
                break;
            case Constant.TYPE_TAG:
                candy.setType(type);
                type = new Type();
                break;
            case Constant.VALUE_TAG:
                candy.setValue(value);
                value = new Value();
                break;
            case Constant.INGREDIENTS_TAG:
                candy.setIngredients(ingredients);
                ingredients = new ArrayList<>();
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        switch (currentElement) {
            case Constant.NAME_TAG:
                candy.setName(new String(ch, start, length));
                break;
            case Constant.ENERGY_TAG:
                candy.setEnergy(Integer.parseInt(new String(ch, start, length)));
                break;
            case Constant.PRODUCTION_TAG:
                candy.setProdution(new String(ch, start, length));
                break;
        }
        parseTypeFields(ch, start, length);
        parseValueFields(ch, start, length);
        parseIngredientsList(ch, start, length);

    }

    private void parseTypeFields(char[] ch, int start, int length) {
        switch (currentElement) {
            case Constant.TYPE_NAME_TAG:
                type.setTypeName(new String(ch, start, length));
                break;
            case Constant.FILLED_TAG:
                type.setFilled(Boolean.parseBoolean(new String(ch, start, length)));
                break;
        }
    }

    private void parseValueFields(char[] ch, int start, int length) {
        switch (currentElement) {
            case Constant.PROTEIN_TAG:
                value.setProteins(Double.parseDouble(new String(ch, start, length)));
                break;
            case Constant.FAT_TAG:
                value.setFat(Double.parseDouble(new String(ch, start, length)));
                break;
            case Constant.CARBOHYDRATES_TAG:
                value.setCarbohydrates(Double.parseDouble(new String(ch, start, length)));
                break;
        }
    }

    private void parseIngredientsList(char[] ch, int start, int length) {
        if (currentElement.equals(Constant.INGREDIENT_NAME)) {
            Ingredient ingredient = new Ingredient();
            ingredient.setIngredientName(new String(ch, start, length));
            ingredients.add(ingredient);
        }
    }
}

