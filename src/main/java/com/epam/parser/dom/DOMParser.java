package com.epam.parser.dom;

import com.epam.model.Candy;
import com.epam.model.Ingredient;
import com.epam.model.Type;
import com.epam.model.Value;
import com.epam.parser.Constant;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMParser {

    public List<Candy> parseDOM(Document document) {
        List<Candy> candies = new ArrayList<>();
        NodeList nodeList = document.getElementsByTagName(Constant.CANDY_TAG);
        for (int index = 0; index < nodeList.getLength(); index++) {
            Candy candy = new Candy();
            Value value;
            Type type;
            Node node = nodeList.item(index);
            Element element = (Element) node;
            type = getType(element.getElementsByTagName(Constant.TYPE_TAG));
            value = getValue(element.getElementsByTagName(Constant.VALUE_TAG));
            setRootElements(element, candy);
            candy.setIngredients(getIngredient(element.getElementsByTagName(Constant.INGREDIENTS_TAG)));
            candy.setType(type);
            candy.setValue(value);
            candies.add(candy);
        }
        return candies;
    }

    private void setRootElements(Element element, Candy candy){
        candy.setCandyId(Integer.parseInt(element.getAttribute(Constant.CANDY_ID_ATTRIBUTE)));
        candy.setName(element.getElementsByTagName(Constant.NAME_TAG).item(0).getTextContent());
        candy.setEnergy(Integer.parseInt(element.getElementsByTagName(Constant.ENERGY_TAG).item(0)
                .getTextContent()));
        candy.setProdution(element.getElementsByTagName(Constant.PRODUCTION_TAG).item(0).getTextContent());
    }

    private Type getType(NodeList nodeList) {
        Type type = new Type();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            type.setTypeName(element.getElementsByTagName(Constant.TYPE_NAME_TAG).item(0)
                    .getTextContent());
            type.setFilled(Boolean.parseBoolean(element.getElementsByTagName(Constant.FILLED_TAG)
                    .item(0).getTextContent()));
        }
        return type;
    }

    private Value getValue(NodeList nodeList) {
        Value value = new Value();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            value.setFat(Double.parseDouble(element.getElementsByTagName(Constant.FAT_TAG)
                    .item(0).getTextContent()));
            value.setCarbohydrates(Double.parseDouble(element.getElementsByTagName(Constant.CARBOHYDRATES_TAG)
                    .item(0).getTextContent()));
            value.setProteins(Double.parseDouble(element.getElementsByTagName(Constant.PROTEIN_TAG)
                    .item(0).getTextContent()));
        }
        return value;
    }

    private List<Ingredient> getIngredient(NodeList nodeList) {
        List<Ingredient> ingredients = new ArrayList<>();
        for (int index = 0; index < nodeList.getLength(); index++) {
            Ingredient ingredient = new Ingredient();
            Node node = nodeList.item(index);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                ingredient.setIngredientName(element.getTextContent());
                ingredients.add(ingredient);
            }
        }
        return ingredients;
    }}
