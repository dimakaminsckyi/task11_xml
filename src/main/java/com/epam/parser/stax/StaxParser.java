package com.epam.parser.stax;

import com.epam.model.Candy;
import com.epam.model.Ingredient;
import com.epam.model.Type;
import com.epam.model.Value;
import com.epam.parser.Constant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StaxParser {

    private static Logger log = LogManager.getLogger(StaxParser.class);
    private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    private List<Candy> candies = new ArrayList<>();
    private Candy candy;
    private Type type;
    private Value value;
    private List<Ingredient> ingredients;

    public List<Candy> parseStax(File xml) {
        try {
            XMLEventReader xmlEventReader = xmlInputFactory
                    .createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    xmlEvent = xmlEventReader.nextEvent();
                    getRootElement(name, startElement, xmlEvent);
                    getTypeElement(name, xmlEvent);
                    getValueElement(name, xmlEvent);
                    getIngredientList(name, xmlEvent);
                }
                endElement(xmlEvent);
            }
        } catch (FileNotFoundException | XMLStreamException el) {
            log.error(el.getMessage());
        }
        return candies;
    }

    private void getTypeElement(String name, XMLEvent xmlEvent) {
        switch (name) {
            case Constant.TYPE_TAG:
                type = new Type();
                break;
            case Constant.TYPE_NAME_TAG:
                type.setTypeName(xmlEvent.asCharacters().getData());
                break;
            case Constant.FILLED_TAG:
                type.setFilled(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                break;
        }
    }

    private void getValueElement(String name, XMLEvent xmlEvent) {
        switch (name) {
            case Constant.VALUE_TAG:
                value = new Value();
                break;
            case Constant.PROTEIN_TAG:
                value.setProteins(Double.parseDouble(xmlEvent.asCharacters().getData()));
                break;
            case Constant.FAT_TAG:
                value.setFat(Double.parseDouble(xmlEvent.asCharacters().getData()));
                break;
            case Constant.CARBOHYDRATES_TAG:
                value.setCarbohydrates(Double.parseDouble(xmlEvent.asCharacters().getData()));
                break;
        }
    }

    private void getIngredientList(String name, XMLEvent xmlEvent) {
        switch (name) {
            case Constant.INGREDIENTS_TAG:
                ingredients = new ArrayList<>();
                break;
            case Constant.INGREDIENT_NAME:
                Ingredient ingredient = new Ingredient();
                ingredient.setIngredientName(xmlEvent.asCharacters().getData());
                ingredients.add(ingredient);
                break;
        }
    }

    private void getRootElement(String name, StartElement startElement, XMLEvent xmlEvent) {
        switch (name) {
            case Constant.CANDY_TAG:
                candy = new Candy();
                Attribute candyId = startElement.getAttributeByName(
                        new QName(Constant.CANDY_ID_ATTRIBUTE));
                if (candyId != null) {
                    candy.setCandyId(Integer.parseInt(candyId.getValue()));
                }
                break;
            case Constant.NAME_TAG:
                candy.setName(xmlEvent.asCharacters().getData());
                break;
            case Constant.ENERGY_TAG:
                candy.setEnergy(Integer.parseInt(xmlEvent.asCharacters().getData()));
                break;
            case Constant.PRODUCTION_TAG:
                candy.setProdution(xmlEvent.asCharacters().getData());
                break;
        }
    }

    private void endElement(XMLEvent xmlEvent) {
        if (xmlEvent.isEndElement()) {
            EndElement endElement = xmlEvent.asEndElement();
            String name = endElement.getName().getLocalPart();
            switch (name) {
                case Constant.CANDY_TAG: {
                    candies.add(candy);
                    candy = null;
                    break;
                }
                case Constant.TYPE_TAG: {
                    candy.setType(type);
                    type = null;
                    break;
                }
                case Constant.VALUE_TAG: {
                    candy.setValue(value);
                    value = null;
                    break;
                }
                case Constant.INGREDIENTS_TAG: {
                    candy.setIngredients(ingredients);
                    ingredients = null;
                    break;
                }
            }
        }
    }
}
