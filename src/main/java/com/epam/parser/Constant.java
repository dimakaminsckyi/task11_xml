package com.epam.parser;

public class Constant {
    public static final String CANDY_TAG = "candy";
    public static final String NAME_TAG = "name";
    public static final String ENERGY_TAG = "energy";
    public static final String PRODUCTION_TAG = "production";
    public static final String TYPE_TAG = "type";
    public static final String VALUE_TAG = "value";
    public static final String INGREDIENTS_TAG = "ingredients";
    public static final String TYPE_NAME_TAG = "typeName";
    public static final String FILLED_TAG = "filled";
    public static final String PROTEIN_TAG = "protein";
    public static final String FAT_TAG = "fat";
    public static final String CARBOHYDRATES_TAG = "carbohydrates";
    public static final String INGREDIENT_NAME = "ingredientName";
    public static final String CANDY_ID_ATTRIBUTE = "candyId";
}
