package com.epam.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

public class HtmlConverter {

    private static Logger log = LogManager.getLogger(HtmlConverter.class);
    private Source xml;
    private Source xslt;

    public HtmlConverter() {
        xml = new StreamSource(new File("src\\main\\resources\\xml\\candies.xml"));
        xslt = new StreamSource("src\\main\\resources\\xml\\candies.xsl");
    }

    public void convertXMLToHTML() {
        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter("src\\main\\resources\\xml\\candies.html");
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer trasform = tFactory.newTransformer(xslt);
            trasform.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
            log.info("Successful");
        } catch (IOException | TransformerFactoryConfigurationError | TransformerException e) {
            e.printStackTrace();
        }
    }
}
