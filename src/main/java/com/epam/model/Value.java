package com.epam.model;

public class Value {

    private double proteins;
    private double fat;
    private double carbohydrates;

    public void setProteins(double proteins) {
        this.proteins = proteins;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public void setCarbohydrates(double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    @Override
    public String toString() {
        return "Value{" +
                "proteins=" + proteins +
                ", fat=" + fat +
                ", carbohydrates=" + carbohydrates +
                '}';
    }
}
