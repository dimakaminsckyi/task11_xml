package com.epam.model;

public class Ingredient {

    private String ingredientName;

    public void setIngredientName(String name) {
        this.ingredientName = name;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "ingredientName='" + ingredientName + '\'' +
                '}';
    }
}
