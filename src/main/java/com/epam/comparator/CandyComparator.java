package com.epam.comparator;

import com.epam.model.Candy;

import java.util.Comparator;

public class CandyComparator implements Comparator<Candy> {
    @Override
    public int compare(Candy o1, Candy o2) {
        return o1.getEnergy() - o2.getEnergy();
    }
}
