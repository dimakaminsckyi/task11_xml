package com.epam.controller;

public interface ParserController {

    void runDOMParser();

    void runSaxParser();

    void runStaxParser();

    void convertToHtml();
}
