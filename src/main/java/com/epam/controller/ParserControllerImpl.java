package com.epam.controller;

import com.epam.comparator.CandyComparator;
import com.epam.model.Candy;
import com.epam.parser.HtmlConverter;
import com.epam.parser.dom.DOMParser;
import com.epam.parser.sax.SaxParser;
import com.epam.parser.stax.StaxParser;
import com.epam.parser.validator.XmlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ParserControllerImpl implements ParserController {

    private static Logger log = LogManager.getLogger(ParserControllerImpl.class);
    private DOMParser domParser;
    private SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    private CandyComparator comparator;
    private File xmlFile = new File("src\\main\\resources\\xml\\candies.xml");
    private File xsdFile = new File("src\\main\\resources\\xml\\candies.xsd");

    public ParserControllerImpl() {
        domParser = new DOMParser();
        comparator = new CandyComparator();
    }

    @Override
    public void runDOMParser() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document document = null;
        try {
            log.info("......DOM.....");
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.parse(xmlFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            log.error(e.getMessage());
        }
        if (XmlValidator.validate(document, xsdFile)) {
            List<Candy> candies = domParser.parseDOM(document);
            candies.sort(comparator);
            log.info(candies);
        } else {
            log.error("XML document failed validation.");
        }
    }

    @Override
    public void runSaxParser() {
        List<Candy> candies = new ArrayList<>();
        try {
            log.info("......SAX.....");
            saxParserFactory.setSchema(XmlValidator.createSchema(xsdFile));
            saxParserFactory.setValidating(true);
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SaxParser saxHandler = new SaxParser();
            saxParser.parse(xmlFile, saxHandler);
            candies = saxHandler.getCandyList();
            candies.sort(comparator);
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            log.error(ex.getMessage());
        }
        log.info(candies);
    }

    @Override
    public void runStaxParser() {
        StaxParser staxParser = new StaxParser();
        log.info("......STAX.....");
        List<Candy> candies = staxParser.parseStax(xmlFile);
        candies.sort(comparator);
        log.info(candies);
    }

    @Override
    public void convertToHtml() {
        HtmlConverter htmlConverter = new HtmlConverter();
        htmlConverter.convertXMLToHTML();
    }
}
