<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 16pt; background-color: #EEE">
                <div style="background-color: red; color: black;">
                    <h2>Candies</h2>
                </div>
                <table border="2">
                    <tr bgcolor="red">
                        <th>Candy ID</th>
                        <th>Name</th>
                        <th>Energy</th>
                        <th>Production</th>
                        <th>Type</th>
                        <th>Filled</th>
                        <th>Ingredients</th>
                        <th>Protein</th>
                        <th>Fat</th>
                        <th>Carbohydrates</th>
                    </tr>

                    <xsl:for-each select="candies/candy">
                        <tr>
                            <td><xsl:value-of select="@candyId" /></td>
                            <td><xsl:value-of select="name"/></td>
                            <td>
                                <xsl:value-of select="energy"/>
                                <xsl:text> kcal</xsl:text>
                            </td>
                            <td><xsl:value-of select="production"/></td>
                            <td><xsl:value-of select="type/typeName"/></td>
                            <td><xsl:value-of select="type/filled"/></td>
                            <td>
                                <xsl:for-each select="ingredients">
                                    <xsl:value-of select="."/>
                                </xsl:for-each>
                            </td>
                            <td>
                                <xsl:value-of select="value/protein"/>
                                <xsl:text> grams</xsl:text>
                            </td>
                            <td>
                                <xsl:value-of select="value/fat"/>
                                <xsl:text> grams</xsl:text>
                            </td>
                            <td>
                                <xsl:value-of select="value/carbohydrates"/>
                                <xsl:text> grams</xsl:text>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>